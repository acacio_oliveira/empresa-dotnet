﻿
using Empresa.Repository.Abstractions;
using Empresa.Repository.DbContex;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Empresa.Repository
{

    public class UsuarioRepository : MySqlContext, IUsuarioRepository
    {
        public UsuarioRepository(DbContextOptions<MySqlContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        public UsuarioModel Obter(string id)
        {
            try
            {
                return Usuarios.FirstOrDefault(x => x.Email == id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
