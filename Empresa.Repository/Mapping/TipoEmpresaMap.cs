﻿using Empresa.Repository.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Empresa.Repository.Mapping
{
    public class TipoEmpresaMap : IEntityTypeConfiguration<TipoEmpresaModel>
    {
        public void Configure(EntityTypeBuilder<TipoEmpresaModel> builder)
        {

            builder.Property(x => x.Nome)
                .HasColumnName("nome")
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(x => x.Sigla)
                .HasColumnName("sigla")
                .HasColumnType("varchar(8)")
                .IsRequired();
        }
    }
}
