﻿using Empresa.Repository.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Repository.Mapping
{

    public class UsuarioMap : IEntityTypeConfiguration<UsuarioModel>
    {
        public void Configure(EntityTypeBuilder<UsuarioModel> builder)
        {
            builder.ToTable("TB_USUARIO").HasKey(x => x.Email);

            builder.Property(x => x.Password)
                .HasColumnName("Password")
                .HasColumnType("varchar(32)")
                .IsRequired();
        }
    }
}
