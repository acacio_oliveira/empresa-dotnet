﻿using Empresa.Repository.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace Empresa.Repository.Mapping
{
    public class EmpresaMap : IEntityTypeConfiguration<EmpresaModel>
    {
        public void Configure(EntityTypeBuilder<EmpresaModel> builder)
        {
            builder.ToTable("TB_EMPRESA").HasKey(x => x.Id);

            builder.Property(x => x.Nome)
                .HasColumnName("nome")
                .HasColumnType("varchar(50)")
                .IsRequired();

            builder.Property(x => x.Cnpj)
                .HasColumnName("numero_cnpj")
                .HasColumnType("varchar(18)")
                .IsRequired();

            builder.Property(x => x.IdTipoEmpresa)
                .HasColumnName("id_tipo_empresa")
                .IsRequired();

            builder.HasOne(x => x.TipoEmpresa)
                .WithMany(x => x.Empresas)
                .HasForeignKey(x => x.IdTipoEmpresa);
        }
    }
}
