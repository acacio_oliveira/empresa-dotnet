﻿using Empresa.Repository.Abstractions;
using Empresa.Repository.DbContex;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Empresa.Repository
{
    public class EmpresaRepository : MySqlContext, IEmpresaRepository
    {
        public EmpresaRepository(DbContextOptions<MySqlContext> contextOptions) : base(contextOptions)
        {
        }

        public IEnumerable<EmpresaModel> Listar()
        {
            var lista = Empresas.ToList();
            return lista;
        }

        public IEnumerable<EmpresaModel> ListarFiltro(string nome, int idTipoEmpresa)
        {
            var lista = Empresas.Where(GetPredicate(nome, idTipoEmpresa));
            return lista;
        }

        public EmpresaModel Obter(int id)
        {
            var empresa = Empresas.FirstOrDefault(c=> c.Id == id);
            return empresa;
        }

        private Expression<Func<EmpresaModel, bool>> GetPredicate(string nome, int idTipoEmpresa)
        {
            if (!string.IsNullOrWhiteSpace(nome) && idTipoEmpresa > 0)
                return p => p.Nome == nome;

            if(!string.IsNullOrWhiteSpace(nome))
                return p => p.Nome == nome;

            if (idTipoEmpresa > 0)
                return p => p.IdTipoEmpresa == idTipoEmpresa;

            return p => false;
        }
    }
}
