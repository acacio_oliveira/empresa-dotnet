﻿
using Empresa.Repository.Abstractions;
using Empresa.Repository.Mapping;
using Microsoft.EntityFrameworkCore;


namespace Empresa.Repository.DbContex
{
    public class MySqlContext : DbContext
    {
        public MySqlContext(DbContextOptions<MySqlContext> contextOptions) : base(contextOptions)
        {

        }

        public DbSet<EmpresaModel> Empresas { get; set; }

        public DbSet<TipoEmpresaModel> TipoEmpresas { get; set; }

        public DbSet<UsuarioModel> Usuarios { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<UsuarioModel>(new UsuarioMap().Configure);
            modelBuilder.Entity<EmpresaModel>(new EmpresaMap().Configure);
            modelBuilder.Entity<TipoEmpresaModel>(new TipoEmpresaMap().Configure);


        }

    }
}
