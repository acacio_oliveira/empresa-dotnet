﻿using Empresa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Service.Abstractions
{
    public interface IEmpresaService
    {
        IEnumerable<EmpresaEntity> Listar();
        EmpresaEntity Obter(int id);
        IEnumerable<EmpresaEntity> ListarFiltro(string nome, int idTipoEmpresa);
    }
}
