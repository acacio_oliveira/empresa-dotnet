﻿using Empresa.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Service.Abstractions
{
    public interface IUsuarioService
    {
        UsuarioEntity Obter(string id);
    }
}
