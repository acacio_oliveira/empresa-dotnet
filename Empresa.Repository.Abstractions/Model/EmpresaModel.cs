﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Repository.Abstractions
{
    public class EmpresaModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public int IdTipoEmpresa { get; set; }
        public TipoEmpresaModel TipoEmpresa { get; set; }
    }
}
