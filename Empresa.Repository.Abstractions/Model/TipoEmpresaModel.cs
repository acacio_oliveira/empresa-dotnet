﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Repository.Abstractions
{
    public class TipoEmpresaModel
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public IEnumerable<EmpresaModel> Empresas { get; set; }
    }
}
