﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Repository.Abstractions
{
    public interface IEmpresaRepository
    {
        IEnumerable<EmpresaModel> Listar();
        EmpresaModel Obter(int id);
        IEnumerable<EmpresaModel> ListarFiltro(string nome, int idTipoEmpresa);
    }
}
