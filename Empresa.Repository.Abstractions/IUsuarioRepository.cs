﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Empresa.Repository.Abstractions
{
    public interface IUsuarioRepository
    {
        UsuarioModel Obter(string id);
    }
}
