﻿
using AutoMapper;
using Empresa.Domain.Entities;
using Empresa.Repository.Abstractions;

namespace Empresa.Util.Profiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<EmpresaModel, EmpresaEntity>().ReverseMap();
            CreateMap<TipoEmpresaModel, TipoEmpresaEntity>().ReverseMap();
            CreateMap<UsuarioModel, UsuarioEntity>().ReverseMap();
        }
    }
}
