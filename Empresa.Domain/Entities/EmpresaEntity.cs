﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Domain.Entities
{
    public class EmpresaEntity
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public int IdTipoEmpresa { get; set; }
        public TipoEmpresaEntity TipoEmpresa { get; set; }
    }
}
