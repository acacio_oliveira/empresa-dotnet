﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Domain.Entities
{
    public class TipoEmpresaEntity
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        public IEnumerable<EmpresaEntity> Empresas { get; set; }
    }
}
