﻿using AutoMapper;
using Empresa.Domain.Entities;
using Empresa.Repository.Abstractions;
using Empresa.Service.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Service
{
    public class EmpresaService : IEmpresaService
    {
        private readonly IEmpresaRepository _empresaRespository;
        private readonly IMapper _mapper;

        public EmpresaService(IEmpresaRepository empresaRepository, IMapper mapper)
        {
            _empresaRespository = empresaRepository;
            _mapper = mapper;
        }

        public IEnumerable<EmpresaEntity> Listar()
        {
            var empresasModel = _empresaRespository.Listar();
            var empresas = _mapper.Map<IEnumerable<EmpresaEntity>>(empresasModel);
            return empresas;
        }

        public IEnumerable<EmpresaEntity> ListarFiltro(string nome, int idTipoEmpresa)
        {
            var empresasModel = _empresaRespository.ListarFiltro(nome, idTipoEmpresa);
            var empresas = _mapper.Map<IEnumerable<EmpresaEntity>>(empresasModel);
            return empresas;
        }

        public EmpresaEntity Obter(int id)
        {
            var empresaModel = _empresaRespository.Obter(id);
            var empresa = _mapper.Map<EmpresaEntity>(empresaModel);
            return empresa;
        }
    }
}
