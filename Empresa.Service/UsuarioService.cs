﻿using AutoMapper;
using Empresa.Domain.Entities;
using Empresa.Repository.Abstractions;
using Empresa.Service.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;

namespace Empresa.Service
{
    public class UsuarioService : IUsuarioService
    {
        private readonly IUsuarioRepository _usurioRepository;
        private readonly IMapper _mapper;

        public UsuarioService(IUsuarioRepository usurioRepository, IMapper mapper)
        {
            _usurioRepository = usurioRepository;
            _mapper = mapper;
        }

        public UsuarioEntity Obter(string id)
        {
            var result = _usurioRepository.Obter(id);
            var usuario = _mapper.Map<UsuarioEntity>(result);

            return usuario;
        }
    }
}
