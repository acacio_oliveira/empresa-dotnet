﻿using Empresa.Domain.Entities;
using Empresa.Service.Abstractions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Empresa.WebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEmpresaService _empresaService;

        public EnterprisesController(IEmpresaService empresaService)
        {
            _empresaService = empresaService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<EmpresaEntity>> Get()
        {
            var lista = _empresaService.Listar();

            return lista.ToArray();
        }

        [Authorize("Bearer")]
        [HttpGet("{id}")]
        public ActionResult<EmpresaEntity> Get(int id)
        {
            var empresa = _empresaService.Obter(id);

            if (empresa == null)
            {
                return NotFound();
            }
            return Ok(empresa);
        }

        [Authorize("Bearer")]
        [HttpGet("{enterprise_types}/{name}")]
        public ActionResult<EmpresaEntity> Get(int enterprise_types, string name)
        {
            var empresa = _empresaService.ListarFiltro(name, enterprise_types);

            if (empresa == null)
            {
                return NotFound();
            }
            return Ok(empresa);
        }

    }
}
