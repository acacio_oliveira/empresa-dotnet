﻿
using Empresa.Repository;
using Empresa.Repository.Abstractions;
using Empresa.Service;
using Empresa.Service.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
namespace APIEmpresa
{

    public static class ServicesExtensions
    {

        public static void RegistrarRepositorios(this IServiceCollection services)
        {
            services.TryAddScoped<IEmpresaRepository, EmpresaRepository>();
            services.TryAddScoped<IUsuarioRepository, UsuarioRepository>();


        }

        public static void RegistrarServicos(this IServiceCollection services)
        {
            services.TryAddScoped<IEmpresaService, EmpresaService>();
            services.TryAddScoped<IUsuarioService, UsuarioService>();

        }
    }
}
